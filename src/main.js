import router from './router'
import Vue from 'vue'
import App from './App'
import 'less/main.less'
import MintUI from 'mint-ui'
import 'mint-ui/lib/style.css'
import 'assets/js/rem'
Vue.use(MintUI)

function setTitle(t) {
  document.title = t;
  var i = document.createElement('iframe');
  i.src = '';
  i.style.display = 'none';
  i.onload = function() {
    setTimeout(function() {
      i.remove();
    }, 5)
  }
  document.body.appendChild(i);
}
router.beforeEach((to, from, next) => {
  setTitle(to.name);
  if (!/Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent)) {
    window.sessionStorage.isPc = 1;
  } else {
    window.sessionStorage.isPc = 0;
  }
  if (to.path == '/user/info') {
    if (window.localStorage.token) {
      next();
    } else {
      next('/user/singin/0');
    }
  } else {
    next();
  }

});
Vue.config.productionTip = false;
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
});
