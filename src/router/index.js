import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

export default new Router({
  routes: [{
      path: '/',
      name: '投注赛事',
      component: (resolve) => require(['../components/home/index'], resolve)
    }, {
      path: '/game/list',
      name: '赛事列表',
      component: (resolve) => require(['../components/game/list'], resolve)
    }, {
      path: '/user/info',
      name: '个人中心',
      component: (resolve) => require(['../components/user/userinfo'], resolve)
    },
    {
      path: '/user/singin/:invited_code',
      name: '用户注册登录',
      component: (resolve) => require(['../components/user/singin'], resolve)
    },
    {
      path: '/user/resetpwd',
      name: '重置密码',
      component: (resolve) => require(['../components/user/resetpwd'], resolve)
    },
    {
      path: '/user/recharge',
      name: '用户充值',
      component: (resolve) => require(['../components/user/recharge'], resolve)
    },
    {
      path: '/user/withdraw',
      name: '申请提现',
      component: (resolve) => require(['../components/user/withdraw'], resolve)
    },
    {
      path: '/user/withdrawList',
      name: '充值提现记录',
      component: (resolve) => require(['../components/user/withdrawList'], resolve)
    },
    {
      path: '/user/profit',
      name: '我的收益',
      component: (resolve) => require(['../components/user/profit'], resolve)
    }
  ]
})
