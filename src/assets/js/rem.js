(function(doc, win) {
  var docEl = doc.documentElement,
    resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
    recalc = function() {
      var clientWidth = doc.body.clientWidth;
      if (!clientWidth) return;
      let fontSize = 12 * (clientWidth / 768);
      if (fontSize < 10) {
        fontSize = 10;
      }
      docEl.style.fontSize = fontSize + 'px';
    };
  if (!doc.addEventListener) return;
  win.addEventListener(resizeEvt, recalc, false);
  doc.addEventListener('DOMContentLoaded', recalc, false);
})(document, window);
