import api_common from 'api/api.common'
import api_baseInfo from 'api/api.baseinfo'
export default {
  register(data) {
    let url = 'user/register';
    return api_common.request('POST', url, data);
  },
  login(data) {
    let url = 'user/login';
    return api_common.request('POST', url, data);
  },
  userInfo() {
    let url = 'user/info';
    return api_common.request('POST', url, { token: api_baseInfo.get_token() });
  },
  resetpwd(data) {
    let url = 'user/resetpwd';
    return api_common.request('POST', url, data);
  }
}
