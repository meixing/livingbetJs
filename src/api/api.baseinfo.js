const CONSTANT = {
};

let baseinfo = {
  pow(amount) {
    if (!amount) {
      return amount;
    }
    return parseFloat((amount / Math.pow(10, 6)).toFixed(6));
  },
  get_token() {
    return window.localStorage.token || '';
  },
  get_constant(constant) {
    if (!constant) {
      return '';
    }
    return CONSTANT[constant];
  },
  get_constant_by_value(constant, value) {
    if (!constant) {
      return '';
    }
    if (!value && value != 0) {
      return '';
    }
    var values = this.get_constant(constant);
    if (values && Array.isArray(values) && values.length) {
      for (var i = 0; i < values.length; i++) {
        if (values[i].code == value) {
          return values[i].text;
        }
      };
    }
  }
}
export default baseinfo;
