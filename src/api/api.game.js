import api_common from 'api/api.common'
import api_md5 from 'api/api.md5'
import api_baseInfo from 'api/api.baseinfo'
export default {
  //比赛列表
  getCompetitionList(res) {
    let url = `competition/list?page=${res.page}&size=${res.size}`,
      data = {
        "token": api_baseInfo.get_token(),
        "cat_id": res.cat_id,
        "timestamp": Math.round(new Date() / 1000),
        "signature": api_md5.getMd5(Math.round(new Date() / 1000))
      }
    return api_common.request('post', url, data);
  },
  getCategoryList(res) {
    let url = `competition/category?size=100&page=0`,
      data = {
        "token": api_baseInfo.get_token(),
        "timestamp": Math.round(new Date() / 1000),
        "signature": api_md5.getMd5(Math.round(new Date() / 1000))
      }
    return api_common.request('post', url, data);
  },
  createBet(res) {
    let url = 'bet/create',
      data = {
        "token": api_baseInfo.get_token(),
        "competition_id": res.competition_id,
        "bet_content": res.bet_content,
        "eth_amount": res.eth_amount,
        "timestamp": Math.round(new Date() / 1000),
        "signature": api_md5.getMd5(Math.round(new Date() / 1000))
      }
    return api_common.request('post', url, data);
  },
  getBetList(res) {
    let url = `bet/list?page=${res.page}&size=${res.size}`,
      data = {
        "token": api_baseInfo.get_token(),
        "timestamp": Math.round(new Date() / 1000),
        "signature": api_md5.getMd5(Math.round(new Date() / 1000))
      }
    return api_common.request('post', url, data);
  },
  withdrawCreate(res) {
    let url = `withdraw/create`,
      data = {
        "eth_amount": res.eth_amount  *Math.pow(10, 6),
        "eth_adress": res.eth_adress,
        "token": api_baseInfo.get_token(),
        "timestamp": Math.round(new Date() / 1000),
        "signature": api_md5.getMd5(Math.round(new Date() / 1000))
      }
    return api_common.request('post', url, data);
  },
  getWithdrawList(res) {
    let url = `money/record?page=${res.page}&size=${res.size}`,
      data = {
        "token": api_baseInfo.get_token(),
        "timestamp": Math.round(new Date() / 1000),
        "signature": api_md5.getMd5(Math.round(new Date() / 1000))
      }
    return api_common.request('post', url, data);
  },
  getprofitList(res) {
    let url = `money/profit?page=${res.page}&size=${res.size}`,
      data = {
        "token": api_baseInfo.get_token()
      }
    return api_common.request('post', url, data);
  }
}
