import axios from 'axios'
import { Indicator, Toast } from 'mint-ui';
import router from '../router/'
export default {
  request(method, url, data) {
    this.showLoading();
    try {
      return new Promise((resolve, reject) => {
        axios({
          "url": url,
          "method": method,
          "baseURL": process.env.NODE_ENV === 'production' ? 'http://api.livingbet.com/api/' : 'http://api.livingbet.com/api/',
          "data": data,
          "headers": {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          },
          "transformRequest": [function(data) {
            let ret = ''
            for (let it in data) {
              ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
            }
            return ret
          }],
          "withCredentials": true,
          "timeout": 5000
        }).then(msg => {
          switch (msg.data.code + '') {
            case '200':
            case '0':
              if (typeof(msg.data.data) == 'string') {
                msg.data.data = JSON.parse(msg.data.data);
              }
              resolve(msg.data);
              break;
            case '-2':
              router.push('/user/singin/0');
              break;
            case '500':
            default:
              if (msg.data && msg.data.msg) {
                Toast(`${msg.data.msg}`);
              } else {
                Toast(`您的网络不稳定，请稍候再试`);
              }
              reject(msg.data);
              break;
          }
          this.hideLoading();
        }).catch(err => {
          switch (err.status) {
            case '500':
              Toast(`您的网络不稳定，请稍候再试`);
              break;
            case '401':
            case '403':
            case '404':
              router.push('/');
              break;
            default:
              Toast(`您的网络不稳定，请稍候再试`);
              break;
          }
          this.hideLoading();
          reject(err);
        });
      });
    } catch (err) {
      this.hideLoading();
      reject(err);
      console.log(e);
    }
  },
  showLoading() {
    if (window.document.querySelector('.mint-indicator')) {
      window.document.querySelector('.mint-indicator').style.display = 'block';
    } else {
      Indicator.open({
        text: '拼命加载中...',
        spinnerType: 'fading-circle'
      });
    }
  },
  hideLoading() {
    if (window.document.querySelector('.mint-indicator')) {
      window.document.querySelector('.mint-indicator').style.display = 'none';
    } else {
      Indicator.close();
    }
  }
}
